package ui;

import javax.swing.JScrollPane;

public class FenetrePrincipale extends Fenetre {

	Classement classement = new Classement();
	
	public FenetrePrincipale() {
		setTitle("Quizzz");
		setSize(400,400);
		add(new JScrollPane(classement));
	}
}
