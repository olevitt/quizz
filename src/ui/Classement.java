package ui;

import java.awt.Color;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import core.Joueur;
import db.JoueurDAO;

public class Classement extends JTable {

	ClassementModel model = new ClassementModel();

	public Classement() {
		setModel(model);
		setBorder(BorderFactory.createLineBorder(Color.black, 1));
		TableRowSorter<TableModel> sorter 
	    = new TableRowSorter<TableModel>(getModel());
		setRowSorter(sorter);
	}



	public class ClassementModel extends DefaultTableModel {

		public ClassementModel() {
			super(new Object[] {"a","b"}, 0);
			JTableHeader header = getTableHeader();
			header.setBackground(Color.yellow);
			List<Joueur> joueurs = JoueurDAO.getListe();
			for (Joueur j : joueurs) {
				addRow(new Object[] {j.getNom(),j.getPoints()});
			}
			this.setRowCount(joueurs.size());
		}
	}
}
