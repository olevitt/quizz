package main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import matisse.Connexion;

import ui.FenetrePrincipale;

public class Main {

	private static Connection con;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new Connexion().setVisible(true);
			}
		});
	}

}