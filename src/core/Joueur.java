package core;

public class Joueur {

	private String nom = "inconnu";
	private int points = 0;
	String pin = "0000";
	int id = -1;
	
	public static Joueur connecte = null;
	
	public Joueur() {
		
	}
	
	public Joueur(int id,String nom, String pin, int points) {
		super();
		this.id = id;
		this.nom = nom;
		this.pin = pin;
		this.points = points;
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	
	public String toString() {
		return nom;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}
	
	
}
