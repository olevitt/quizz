package core;

import java.util.Date;

public class Transaction {

	
	String raison;
	String nomJoueur;
	Date date;
	int montant;
	
	
	
	public Transaction(String raison, String nomJoueur, Date date, int montant) {
		super();
		this.raison = raison;
		this.nomJoueur = nomJoueur;
		this.date = date;
		this.montant = montant;
	}
	
	public String getRaison() {
		return raison;
	}
	public void setRaison(String raison) {
		this.raison = raison;
	}
	public String getNomJoueur() {
		return nomJoueur;
	}
	public void setIdJoueur(String nomJoueur) {
		this.nomJoueur = nomJoueur;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getMontant() {
		return montant;
	}
	public void setMontant(int montant) {
		this.montant = montant;
	}
	
	
}
