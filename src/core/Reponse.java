package core;

public class Reponse {

	int jetonsA,jetonsB,jetonsC,jetonsD;

	public Reponse(int jetonsA, int jetonsB, int jetonsC, int jetonsD) {
		super();
		this.jetonsA = jetonsA;
		this.jetonsB = jetonsB;
		this.jetonsC = jetonsC;
		this.jetonsD = jetonsD;
	}

	public int getJetonsA() {
		return jetonsA;
	}

	public void setJetonsA(int jetonsA) {
		this.jetonsA = jetonsA;
	}

	public int getJetonsB() {
		return jetonsB;
	}

	public void setJetonsB(int jetonsB) {
		this.jetonsB = jetonsB;
	}

	public int getJetonsC() {
		return jetonsC;
	}

	public void setJetonsC(int jetonsC) {
		this.jetonsC = jetonsC;
	}

	public int getJetonsD() {
		return jetonsD;
	}

	public void setJetonsD(int jetonsD) {
		this.jetonsD = jetonsD;
	}
	
	
}
