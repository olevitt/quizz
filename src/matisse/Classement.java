/*
 * Classement.java
 *
 * Created on __DATE__, __TIME__
 */

package matisse;

import java.io.FileWriter;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import au.com.bytecode.opencsv.CSVWriter;
import core.Joueur;
import csv.CSV;
import db.JoueurDAO;

/**
 *
 * @author  __USER__
 */
public class Classement extends javax.swing.JFrame {

	List<Joueur> joueurs;

	/** Creates new form Classement */
	public Classement() {
		initComponents();

		DefaultTableModel model = ((DefaultTableModel) jTable1.getModel());
		String[] columns = { "Place", "Nom", "Etincelles" };
		model.setColumnIdentifiers(columns);
		while (model.getRowCount() > 0) {
			model.removeRow(0);
		}
		joueurs = JoueurDAO.getListe(JoueurDAO.POINTS, JoueurDAO.DESC);
		int i = 1;
		for (Joueur j : joueurs) {
			model.addRow(new Object[] { i, j.getNom(), j.getPoints() });
			i++;
		}
		
		setLocationRelativeTo(null);
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jScrollPane1 = new javax.swing.JScrollPane();
		jTable1 = new javax.swing.JTable();
		jButton1 = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		jTable1.setModel(new javax.swing.table.DefaultTableModel(
				new Object[][] {

				}, new String[] { "Place", "Nom", "Points" }) {
			Class[] types = new Class[] { java.lang.Integer.class,
					java.lang.String.class, java.lang.Integer.class };
			boolean[] canEdit = new boolean[] { false, false, false };

			public Class getColumnClass(int columnIndex) {
				return types[columnIndex];
			}

			public boolean isCellEditable(int rowIndex, int columnIndex) {
				return canEdit[columnIndex];
			}
		});
		jScrollPane1.setViewportView(jTable1);

		jButton1.setText("Fermer");
		jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton1ActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(jScrollPane1,
						javax.swing.GroupLayout.DEFAULT_SIZE, 632,
						Short.MAX_VALUE)
				.addGroup(
						layout.createSequentialGroup().addGap(263, 263, 263)
								.addComponent(jButton1)
								.addContainerGap(284, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addContainerGap()
								.addComponent(jScrollPane1,
										javax.swing.GroupLayout.PREFERRED_SIZE,
										384,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.RELATED,
										12, Short.MAX_VALUE)
								.addComponent(jButton1).addContainerGap()));

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new Connexion().setVisible(true);
				dispose();
			}
		});
	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new Classement().setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JButton jButton1;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JTable jTable1;
	// End of variables declaration//GEN-END:variables

}