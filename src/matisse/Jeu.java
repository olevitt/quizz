/*
 * Jeu.java
 *
 * Created on __DATE__, __TIME__
 */

package matisse;

import java.awt.event.ActionEvent;
import java.text.SimpleDateFormat;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import preferences.Prefs;

import core.Joueur;
import core.Quizz;
import core.Reponse;
import db.JoueurDAO;
import db.QuizzDao;
import db.ReponseDAO;

/**
 *
 * @author  __USER__
 */
public class Jeu extends javax.swing.JFrame {

	Quizz quizz;

	/** Creates new form Jeu */
	public Jeu() {
		initComponents();
		quizz = QuizzDao.getQuizzDuJour();
		if (quizz == null) {
			JOptionPane.showMessageDialog(this,
					"Il n'y a plus de question en stock :( Previens Sami !");
			deco();
			return;
		}
		jTextArea1.setText(quizz.getQuestion());
		jLabel2.setText(quizz.getReponse1());
		jLabel4.setText(quizz.getReponse2());
		jLabel5.setText(quizz.getReponse3());
		jLabel10.setText(quizz.getReponse4());
		labelDate.setText("Question du "
				+ new SimpleDateFormat("dd/MM/yy").format(quizz.getDate()));
		Reponse reponse = ReponseDAO.getReponse(Joueur.connecte.getId(),
				quizz.getId());
		if (reponse != null) {
			/*jTextField1.setText(""+reponse.getJetonsA());
			jTextField3.setText(""+reponse.getJetonsB());
			jTextField4.setText(""+reponse.getJetonsC());
			jTextField5.setText(""+reponse.getJetonsD());
			setNbJetonsRestants(4-reponse.getJetonsA()-reponse.getJetonsB()-reponse.getJetonsC()-reponse.getJetonsD());
			 */
			JOptionPane
					.showMessageDialog(this,
							"Tu as déjà validé le quizz du jour, \"validé c'est validé !\"");
			deco();
			return;
		}

		cinquante50.setVisible(Prefs.prefs.getBoolean(Prefs.CHECKBOX5050,true));
		jButton12.setVisible(Prefs.prefs.getBoolean(Prefs.CHECKBOXPUBLIC,true));
		jokers.setVisible(cinquante50.isVisible() || jButton12.isVisible());
		refreshTitle();
		setLocationRelativeTo(null);
	}

	private void refresh() {
		Joueur.connecte = JoueurDAO.getById(Joueur.connecte.getId());
		refreshTitle();
	}

	private void refreshTitle() {
		setTitle(Joueur.connecte.getNom() + " (" + Joueur.connecte.getPoints()
				+ " étincelles)");
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jLabel6 = new javax.swing.JLabel();
		jLabel7 = new javax.swing.JLabel();
		jButton2 = new javax.swing.JButton();
		jPanel1 = new javax.swing.JPanel();
		jLabel2 = new javax.swing.JLabel();
		jButton3 = new javax.swing.JButton();
		jTextField1 = new javax.swing.JTextField();
		jButton1 = new javax.swing.JButton();
		jScrollPane1 = new javax.swing.JScrollPane();
		jTextArea1 = new javax.swing.JTextArea();
		jPanel3 = new javax.swing.JPanel();
		jLabel4 = new javax.swing.JLabel();
		jButton6 = new javax.swing.JButton();
		jTextField3 = new javax.swing.JTextField();
		jButton7 = new javax.swing.JButton();
		jPanel4 = new javax.swing.JPanel();
		jLabel5 = new javax.swing.JLabel();
		jButton8 = new javax.swing.JButton();
		jTextField4 = new javax.swing.JTextField();
		jButton9 = new javax.swing.JButton();
		jPanel5 = new javax.swing.JPanel();
		jLabel10 = new javax.swing.JLabel();
		jButton10 = new javax.swing.JButton();
		jTextField5 = new javax.swing.JTextField();
		jButton11 = new javax.swing.JButton();
		jButton4 = new javax.swing.JButton();
		labelDate = new javax.swing.JLabel();
		jokers = new javax.swing.JPanel();
		jButton12 = new javax.swing.JButton();
		cinquante50 = new javax.swing.JButton();
		jLabel1 = new javax.swing.JLabel();

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		jLabel6.setText("Nombre de jetons restants : ");

		jLabel7.setFont(new java.awt.Font("Ubuntu", 0, 24));
		jLabel7.setText("4");

		jButton2.setText("Valider");
		jButton2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton2ActionPerformed(evt);
			}
		});

		jLabel2.setText("R\u00e9ponse A");

		jButton3.setText("-");
		jButton3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton3ActionPerformed(evt);
			}
		});

		jTextField1.setEditable(false);
		jTextField1.setFont(new java.awt.Font("Ubuntu", 1, 24));
		jTextField1.setForeground(new java.awt.Color(0, 0, 0));
		jTextField1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
		jTextField1.setText("0");
		jTextField1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jTextField1ActionPerformed(evt);
			}
		});

		jButton1.setText("+");
		jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton1ActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout
				.setHorizontalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								javax.swing.GroupLayout.Alignment.TRAILING,
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(
												jLabel2,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												510, Short.MAX_VALUE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(jButton3)
										.addGap(18, 18, 18)
										.addComponent(
												jTextField1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												25,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(21, 21, 21)
										.addComponent(jButton1)
										.addContainerGap()));
		jPanel1Layout
				.setVerticalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																jPanel1Layout
																		.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.BASELINE)
																		.addComponent(
																				jLabel2,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				57,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addComponent(
																				jButton1,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				25,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addComponent(
																				jButton3,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				25,
																				javax.swing.GroupLayout.PREFERRED_SIZE))
														.addGroup(
																jPanel1Layout
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				jTextField1,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				31,
																				javax.swing.GroupLayout.PREFERRED_SIZE)))
										.addContainerGap(
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)));

		jTextArea1.setColumns(20);
		jTextArea1.setEditable(false);
		jTextArea1.setLineWrap(true);
		jTextArea1.setRows(5);
		jTextArea1
				.setText("Texte ultra long ............................................................. mais genre super super super long :)fdsfdsfdsfsdfdsfsfdsfdksgfdjgfjhfghjfghlfgkhfghkgflhgfhkgfhlghgofhgfhlfkhfglhfghkgfhlfglhgfhkgfhfglhfghfgkhflhflhgfkhfglhlfhlgfkezrperoezrlezremzrlzerezkrkzekrkzekrezkrkzekrekkrkerkekrkekrekrkekrekrkekrekrkekrekrkekrekrkekrekrkekrekrkekrekrkekrkekrekrkekrekrkerkekrkrkerkekrkerkkrekrkerekr ");
		jTextArea1.setWrapStyleWord(true);
		jScrollPane1.setViewportView(jTextArea1);

		jLabel4.setText("R\u00e9ponse B");

		jButton6.setText("-");
		jButton6.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton6ActionPerformed(evt);
			}
		});

		jTextField3.setEditable(false);
		jTextField3.setFont(new java.awt.Font("Ubuntu", 1, 24));
		jTextField3.setForeground(new java.awt.Color(0, 0, 0));
		jTextField3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
		jTextField3.setText("0");
		jTextField3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jTextField3ActionPerformed(evt);
			}
		});

		jButton7.setText("+");
		jButton7.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton7ActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(
				jPanel3);
		jPanel3.setLayout(jPanel3Layout);
		jPanel3Layout
				.setHorizontalGroup(jPanel3Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								javax.swing.GroupLayout.Alignment.TRAILING,
								jPanel3Layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(
												jLabel4,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												510, Short.MAX_VALUE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(jButton6)
										.addGap(18, 18, 18)
										.addComponent(
												jTextField3,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												25,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(21, 21, 21)
										.addComponent(jButton7)
										.addContainerGap()));
		jPanel3Layout
				.setVerticalGroup(jPanel3Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel3Layout
										.createSequentialGroup()
										.addGroup(
												jPanel3Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																jPanel3Layout
																		.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.BASELINE)
																		.addComponent(
																				jLabel4,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				57,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addComponent(
																				jButton7,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				25,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addComponent(
																				jButton6,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				25,
																				javax.swing.GroupLayout.PREFERRED_SIZE))
														.addGroup(
																jPanel3Layout
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				jTextField3,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				31,
																				javax.swing.GroupLayout.PREFERRED_SIZE)))
										.addContainerGap(
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)));

		jLabel5.setText("R\u00e9ponse C");

		jButton8.setText("-");
		jButton8.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton8ActionPerformed(evt);
			}
		});

		jTextField4.setEditable(false);
		jTextField4.setFont(new java.awt.Font("Ubuntu", 1, 24));
		jTextField4.setForeground(new java.awt.Color(0, 0, 0));
		jTextField4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
		jTextField4.setText("0");
		jTextField4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jTextField4ActionPerformed(evt);
			}
		});

		jButton9.setText("+");
		jButton9.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton9ActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(
				jPanel4);
		jPanel4.setLayout(jPanel4Layout);
		jPanel4Layout
				.setHorizontalGroup(jPanel4Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								javax.swing.GroupLayout.Alignment.TRAILING,
								jPanel4Layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(
												jLabel5,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												510, Short.MAX_VALUE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(jButton8)
										.addGap(18, 18, 18)
										.addComponent(
												jTextField4,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												25,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(21, 21, 21)
										.addComponent(jButton9)
										.addContainerGap()));
		jPanel4Layout
				.setVerticalGroup(jPanel4Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel4Layout
										.createSequentialGroup()
										.addGroup(
												jPanel4Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																jPanel4Layout
																		.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.BASELINE)
																		.addComponent(
																				jLabel5,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				57,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addComponent(
																				jButton9,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				25,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addComponent(
																				jButton8,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				25,
																				javax.swing.GroupLayout.PREFERRED_SIZE))
														.addGroup(
																jPanel4Layout
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				jTextField4,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				31,
																				javax.swing.GroupLayout.PREFERRED_SIZE)))
										.addContainerGap(
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)));

		jLabel10.setText("R\u00e9ponse D");

		jButton10.setText("-");
		jButton10.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton10ActionPerformed(evt);
			}
		});

		jTextField5.setEditable(false);
		jTextField5.setFont(new java.awt.Font("Ubuntu", 1, 24));
		jTextField5.setForeground(new java.awt.Color(0, 0, 0));
		jTextField5.setHorizontalAlignment(javax.swing.JTextField.CENTER);
		jTextField5.setText("0");
		jTextField5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jTextField5ActionPerformed(evt);
			}
		});

		jButton11.setText("+");
		jButton11.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton11ActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(
				jPanel5);
		jPanel5.setLayout(jPanel5Layout);
		jPanel5Layout
				.setHorizontalGroup(jPanel5Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								javax.swing.GroupLayout.Alignment.TRAILING,
								jPanel5Layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(
												jLabel10,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												510, Short.MAX_VALUE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(jButton10)
										.addGap(18, 18, 18)
										.addComponent(
												jTextField5,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												25,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(21, 21, 21)
										.addComponent(jButton11)
										.addContainerGap()));
		jPanel5Layout
				.setVerticalGroup(jPanel5Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel5Layout
										.createSequentialGroup()
										.addGroup(
												jPanel5Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																jPanel5Layout
																		.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.BASELINE)
																		.addComponent(
																				jLabel10,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				57,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addComponent(
																				jButton11,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				25,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addComponent(
																				jButton10,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				25,
																				javax.swing.GroupLayout.PREFERRED_SIZE))
														.addGroup(
																jPanel5Layout
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				jTextField5,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				31,
																				javax.swing.GroupLayout.PREFERRED_SIZE)))
										.addContainerGap(
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)));

		jButton4.setText("D\u00e9connexion");
		jButton4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton4ActionPerformed(evt);
			}
		});

		labelDate.setText("Question du xx/xx/xx");

		jButton12.setText("Avis du public");
		jButton12.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton12ActionPerformed(evt);
			}
		});

		cinquante50.setText("50/50");
		cinquante50.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cinquante50ActionPerformed(evt);
			}
		});

		jLabel1.setText("Jokers :");

		javax.swing.GroupLayout jokersLayout = new javax.swing.GroupLayout(
				jokers);
		jokers.setLayout(jokersLayout);
		jokersLayout.setHorizontalGroup(jokersLayout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				jokersLayout.createSequentialGroup().addGap(23, 23, 23)
						.addComponent(jLabel1).addGap(191, 191, 191)
						.addComponent(cinquante50).addGap(68, 68, 68)
						.addComponent(jButton12)
						.addContainerGap(170, Short.MAX_VALUE)));
		jokersLayout
				.setVerticalGroup(jokersLayout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								javax.swing.GroupLayout.Alignment.TRAILING,
								jokersLayout
										.createSequentialGroup()
										.addContainerGap(
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)
										.addGroup(
												jokersLayout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(jLabel1)
														.addComponent(
																cinquante50)
														.addComponent(jButton12))
										.addGap(65, 65, 65)));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING)
												.addGroup(
														layout.createSequentialGroup()
																.addContainerGap()
																.addComponent(
																		jPanel1,
																		javax.swing.GroupLayout.DEFAULT_SIZE,
																		javax.swing.GroupLayout.DEFAULT_SIZE,
																		Short.MAX_VALUE))
												.addGroup(
														layout.createSequentialGroup()
																.addGap(21, 21,
																		21)
																.addComponent(
																		jScrollPane1,
																		javax.swing.GroupLayout.DEFAULT_SIZE,
																		683,
																		Short.MAX_VALUE))
												.addGroup(
														javax.swing.GroupLayout.Alignment.TRAILING,
														layout.createSequentialGroup()
																.addContainerGap()
																.addComponent(
																		jPanel3,
																		javax.swing.GroupLayout.DEFAULT_SIZE,
																		javax.swing.GroupLayout.DEFAULT_SIZE,
																		Short.MAX_VALUE))
												.addGroup(
														javax.swing.GroupLayout.Alignment.TRAILING,
														layout.createSequentialGroup()
																.addContainerGap()
																.addComponent(
																		jPanel4,
																		javax.swing.GroupLayout.DEFAULT_SIZE,
																		javax.swing.GroupLayout.DEFAULT_SIZE,
																		Short.MAX_VALUE))
												.addGroup(
														layout.createSequentialGroup()
																.addContainerGap()
																.addGroup(
																		layout.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.LEADING)
																				.addComponent(
																						jPanel5,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						Short.MAX_VALUE)
																				.addGroup(
																						layout.createSequentialGroup()
																								.addGap(12,
																										12,
																										12)
																								.addComponent(
																										jLabel6)
																								.addGap(262,
																										262,
																										262)))))
								.addContainerGap())
				.addGroup(
						javax.swing.GroupLayout.Alignment.TRAILING,
						layout.createSequentialGroup()
								.addContainerGap(605, Short.MAX_VALUE)
								.addComponent(jLabel7).addGap(97, 97, 97))
				.addGroup(
						layout.createSequentialGroup()
								.addComponent(jokers,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE).addContainerGap())
				.addGroup(
						layout.createSequentialGroup()
								.addContainerGap()
								.addComponent(jButton4)
								.addGap(169, 169, 169)
								.addComponent(labelDate)
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.RELATED,
										115, Short.MAX_VALUE)
								.addComponent(jButton2).addGap(63, 63, 63)));
		layout.setVerticalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addContainerGap()
								.addComponent(jScrollPane1,
										javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addGap(15, 15, 15)
								.addComponent(jPanel1,
										javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(jPanel3,
										javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(jPanel4,
										javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(jPanel5,
										javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addGap(6, 6, 6)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING)
												.addComponent(jLabel6)
												.addComponent(jLabel7))
								.addGap(21, 21, 21)
								.addComponent(jokers,
										javax.swing.GroupLayout.PREFERRED_SIZE,
										57,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jButton4)
												.addComponent(labelDate)
												.addComponent(jButton2))
								.addContainerGap(
										javax.swing.GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)));

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {
		int choix = JOptionPane.showConfirmDialog(
				this,
				"L'avis du public coûte "
						+ Prefs.prefs.getInt(Prefs.AVISDUPUBLIC, 200)
						+ " étincelles", "Confirmation",
				JOptionPane.YES_NO_OPTION);
		if (choix == JOptionPane.OK_OPTION) {
			int nb = ReponseDAO.getNbReponses(quizz.getId());
			if (nb < Prefs.prefs.getInt(Prefs.MINPUBLIC,5)) {
				int choix2 = JOptionPane.showConfirmDialog(
						this,
						"Il n'y a que "
								+ nb
								+ " personnes qui ont déjà répondu au quizz du jour ...\n" +
								"Es tu sûr qu'il est malin de demander l'avis du public ?", "Confirmation",
						JOptionPane.YES_NO_OPTION);
				if (choix2 != JOptionPane.OK_OPTION) 
					return;
			}
			JoueurDAO.ajouterPoints(Joueur.connecte.getId(),
					-Prefs.prefs.getInt(Prefs.AVISDUPUBLIC, 200),"Utilisation de l'avis du public");
			refresh();
			JOptionPane.showMessageDialog(this,
					ReponseDAO.getAvisPublic(quizz.getId()), "Avis du public",
					JOptionPane.INFORMATION_MESSAGE);
		}
	}

	private void cinquante50ActionPerformed(java.awt.event.ActionEvent evt) {
		int choix = JOptionPane.showConfirmDialog(this, "Le 50/50 coûte "
				+ Prefs.prefs.getInt(Prefs.CINQUANTECINQUANTE, 250)
				+ " étincelles", "Confirmation", JOptionPane.YES_NO_OPTION);
		if (choix == JOptionPane.OK_OPTION) {
			JoueurDAO.ajouterPoints(Joueur.connecte.getId(),
					-Prefs.prefs.getInt(Prefs.CINQUANTECINQUANTE, 250),"Utilisation du 50/50");
			cinquanteCinquante();
			refresh();
		}
	}

	public void cinquanteCinquante() {
		cinquante50.setEnabled(false);
		jTextField1.setText("0");
		jTextField3.setText("0");
		jTextField4.setText("0");
		jTextField5.setText("0");

		int bonneReponse = quizz.getBonneReponse();
		boolean[] visible = new boolean[] { false, false, false, false };
		visible[bonneReponse - 1] = true;
		int random;
		while (visible[(random = (int) (Math.random() * 3))])
			random = 2; //do nothing SO UGLY
		visible[random] = true;
		jPanel1.setVisible(visible[0]);
		jPanel3.setVisible(visible[1]);
		jPanel4.setVisible(visible[2]);
		jPanel5.setVisible(visible[3]);
		setNbJetonsRestants(4);
	}

	protected void jTextField5ActionPerformed(ActionEvent evt) {
		// TODO Auto-generated method stub

	}

	protected void jTextField4ActionPerformed(ActionEvent evt) {
		// TODO Auto-generated method stub

	}

	protected void jTextField3ActionPerformed(ActionEvent evt) {
		// TODO Auto-generated method stub

	}

	protected void jTextField1ActionPerformed(ActionEvent evt) {
		// TODO Auto-generated method stub

	}

	protected void jButton2ActionPerformed(ActionEvent evt) {
		// TODO Auto-generated method stub
		if (getNbJetonsRestants() == 4) {
			int choix = JOptionPane
					.showConfirmDialog(
							this,
							"Tu n'as utilisé aucun jeton, es tu sûr que c'est ce que tu veux faire ?",
							"Confirmation", JOptionPane.YES_NO_OPTION);
			if (choix != JOptionPane.YES_OPTION)
				return;
		}
		ReponseDAO.ajouterReponse(Joueur.connecte.getId(), quizz.getId(),
				getJeton(1), getJeton(2), getJeton(3), getJeton(4));
		JOptionPane.showMessageDialog(this, "Choix pris en compte, merci !");
		deco();
	}

	public int getJeton(int quel) {
		JTextField text = jTextField1;
		if (quel == 2)
			text = jTextField3;
		if (quel == 3)
			text = jTextField4;
		if (quel == 4)
			text = jTextField5;
		return Integer.parseInt(text.getText());
	}

	private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		deco();
	}

	private void deco() {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				Joueur.connecte = null;
				new Connexion().setVisible(true);
				Jeu.this.dispose();
			}
		});
	}

	private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		changerJeton(jTextField5, 1);
	}

	private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		changerJeton(jTextField5, -1);
	}

	private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		changerJeton(jTextField4, 1);
	}

	private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		changerJeton(jTextField4, -1);
	}

	private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		changerJeton(jTextField3, 1);
	}

	private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		changerJeton(jTextField3, -1);
	}

	private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		changerJeton(jTextField1, -1);
	}

	private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		changerJeton(jTextField1, 1);
	}

	public void changerJeton(JTextField text, int nb) {
		int jetonsPresents = Integer.parseInt(text.getText());
		int restants = getNbJetonsRestants();
		if (nb > restants)
			return;
		if (jetonsPresents == 0 && nb == -1)
			return;
		setNbJetonsRestants(restants - nb);
		text.setText((jetonsPresents + nb) + "");
	}

	public int getNbJetonsRestants() {
		return Integer.parseInt(jLabel7.getText());
	}

	public void setNbJetonsRestants(int nb) {
		jLabel7.setText("" + nb);
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JButton cinquante50;
	private javax.swing.JButton jButton1;
	private javax.swing.JButton jButton10;
	private javax.swing.JButton jButton11;
	private javax.swing.JButton jButton12;
	private javax.swing.JButton jButton2;
	private javax.swing.JButton jButton3;
	private javax.swing.JButton jButton4;
	private javax.swing.JButton jButton6;
	private javax.swing.JButton jButton7;
	private javax.swing.JButton jButton8;
	private javax.swing.JButton jButton9;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel10;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JLabel jLabel6;
	private javax.swing.JLabel jLabel7;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JPanel jPanel3;
	private javax.swing.JPanel jPanel4;
	private javax.swing.JPanel jPanel5;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JTextArea jTextArea1;
	private javax.swing.JTextField jTextField1;
	private javax.swing.JTextField jTextField3;
	private javax.swing.JTextField jTextField4;
	private javax.swing.JTextField jTextField5;
	private javax.swing.JPanel jokers;
	private javax.swing.JLabel labelDate;
	// End of variables declaration//GEN-END:variables

}