package db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import preferences.Prefs;

import core.Quizz;

public class QuizzDao {

	public static Quizz getQuizzDuJour() {
		try {
			Statement stat = DB.getConnection().createStatement();
			ResultSet res = stat.executeQuery("select * from quizz ORDER BY jour");
			Calendar debut = getDebut();
			while (res.next()) {
				Quizz quizz = creerQuizz(res);
				if (quizz.getDate().getTime() >= debut.getTime().getTime()) {
					return quizz;
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Calendar getDebut() {
		Calendar debut = new GregorianCalendar();
		debut.set(Calendar.HOUR_OF_DAY, 0);
		debut.set(Calendar.MINUTE,0);
		debut.set(Calendar.SECOND, 0);
		debut.set(Calendar.MILLISECOND, 0);
		return debut;
	}
	
	public static Quizz getById(int id) {
		try {
			Statement stat = DB.getConnection().createStatement();
			ResultSet res = stat.executeQuery("select * from quizz WHERE id = "+id);
			while (res.next()) {
				return creerQuizz(res);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static List<Quizz> getListe() {
		List<Quizz> retour = new ArrayList<Quizz>();
		try {
			Statement stat = DB.getConnection().createStatement();
			ResultSet res = stat.executeQuery("select * from quizz ORDER BY jour DESC");
			while (res.next()) {
				retour.add(creerQuizz(res));
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return retour;
	}
	
	public static void creerQuestion() {
		try {
			PreparedStatement prep = DB.getConnection()
					.prepareStatement("insert into quizz(jour,question,reponsea,reponseb,reponsec,reponsed,bonnereponse) values(?,?,?,?,?,?,?);");
			prep.setDate(1, new java.sql.Date(new Date().getTime()));
			prep.setString(2, "2+2 ?");
			prep.setString(3,"A");
			prep.setString(4,"B");
			prep.setString(5,"Obi-wan kenobi");
			prep.setString(6,"La réponse D");
			prep.setInt(7,4);
			prep.execute();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static Quizz creerQuizz(ResultSet res) throws SQLException {
		return new Quizz(res.getInt("id"),
				res.getString("question"),
				res.getString("reponsea"),
				res.getString("reponseb"),
				res.getString("reponsec"),
				res.getString("reponsed"),
				res.getInt("bonneReponse"),
				res.getDate("jour"));
	}
	
	public static void insertOrUpdate(Quizz q) {
		try {
			PreparedStatement prep = DB.getConnection()
					.prepareStatement("replace into quizz(jour,question,reponsea,reponseb,reponsec,reponsed,bonnereponse,id) values(?,?,?,?,?,?,?,?);");
			prep.setDate(1, new java.sql.Date(q.getDate().getTime()));
			prep.setString(2, q.getQuestion());
			prep.setString(3, q.getReponse1());
			prep.setString(4, q.getReponse2());
			prep.setString(5, q.getReponse3());
			prep.setString(6, q.getReponse4());
			prep.setInt(7, q.getBonneReponse());
			prep.setInt(8, q.getId());
			prep.execute();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
			
	}
	
	public static void traitementQuizz(int question) {
		Quizz quizz = QuizzDao.getById(question);
		if (quizz == null) {
			System.out.println("Quizz inconnu :(");
			return;
		}
		if (TraitementDAO.isTraite(question)) {
			System.out.println("Quizz déjà traité :(");
			return;
		}
		
		try {
			Statement stat = DB.getConnection().createStatement();
			ResultSet res = stat.executeQuery("select reponses.idJoueur,jetonsA,jetonsB,jetonsC,jetonsD from reponses, joueurs WHERE joueurs.id = reponses.idJoueur AND idQuestion = "+question);
			while (res.next()) {
				int idJoueur = res.getInt(1);
				int nbEtincelles = 0;
				for (int i = 1; i < 5; i++) {
					if (quizz.getBonneReponse() == i) {
						nbEtincelles += Prefs.prefs.getInt(Prefs.BONNEREPONSE, 100) * res.getInt(i+1);
					}
					else {
						nbEtincelles += Prefs.prefs.getInt(Prefs.MAUVAISEREPONSE, -50) * res.getInt(i+1);
					}
				}
				JoueurDAO.ajouterPoints(idJoueur, nbEtincelles,"Quizz du "+new SimpleDateFormat("dd/MM/yy").format(quizz.getDate()));
			}
			
			TraitementDAO.setTraite(question);
		}
		catch (Exception e) {
			System.out.println("Traitement impossible :(");
			e.printStackTrace();
		}
		
		
		
	}
	
	
}
