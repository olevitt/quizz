package db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import core.Reponse;

public class ReponseDAO {

	public static Reponse getReponse(int joueur,int question) {
		try {
			return creerReponse(DB.getConnection().createStatement().executeQuery("SELECT * FROM reponses WHERE idJoueur = "+joueur+" AND idQuestion = "+question));
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int getNbReponses(int quizzId) {
		try {
			ResultSet res = DB.getConnection().createStatement().executeQuery("SELECT COUNT(*) FROM reponses WHERE idQuestion = "+quizzId);
			res.next();
			return res.getInt(1);
		}
		catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public static String getAvisPublic(int idQuestion) {
		try {
			ResultSet result = DB.getConnection().createStatement().executeQuery("SELECT SUM(jetonsA) as jetonA, SUM(jetonsB) as jetonB, SUM(jetonsC) as jetonC, SUM(jetonsD) as jetonD FROM reponses WHERE idQuestion = "+idQuestion);
			result.next();
			StringBuilder builder = new StringBuilder();
			int jetonsA = result.getInt(1);
			int jetonsB = result.getInt(2);
			int jetonsC = result.getInt(3);
			int jetonsD = result.getInt(4);
			int jetonsTotal = jetonsA + jetonsB + jetonsC + jetonsD;
			if (jetonsTotal == 0) 
				jetonsTotal = 1;
			builder.append("Réponse A : "+jetonsA+" ("+Math.round(jetonsA*100/jetonsTotal)+"%)"+System.getProperty("line.separator"));
			builder.append("Réponse B : "+jetonsB+" ("+Math.round(jetonsB*100/jetonsTotal)+"%)"+System.getProperty("line.separator"));
			builder.append("Réponse C : "+jetonsB+" ("+Math.round(jetonsC*100/jetonsTotal)+"%)"+System.getProperty("line.separator"));
			builder.append("Réponse D : "+jetonsB+" ("+Math.round(jetonsD*100/jetonsTotal)+"%)"+System.getProperty("line.separator"));
			return builder.toString();	
		}
		catch (Exception e) {
			return "";
		}
	}
	
	private static Reponse creerReponse(ResultSet res) {
		try {
			res.next();
			return new Reponse(res.getInt("jetonsA"),res.getInt("jetonsB"),res.getInt("jetonsC"),res.getInt("jetonsD"));
		}
		catch (Exception e) {
			return null;
		}
	}
	
	public static void ajouterReponse(int joueur,int question,int jetonA,int jetonB,int jetonC,int jetonD) {
		try {
			PreparedStatement prep = DB.getConnection()
					.prepareStatement("replace into reponses(idJoueur,idQuestion,jetonsA,jetonsB,jetonsC,jetonsD) values(?,?,?,?,?,?);");
			prep.setInt(1, joueur);
			prep.setInt(2, question);
			prep.setInt(3, jetonA);
			prep.setInt(4, jetonB);
			prep.setInt(5, jetonC);
			prep.setInt(6, jetonD);
			prep.execute();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
