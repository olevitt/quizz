package db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import preferences.Prefs;

import core.Joueur;

public class JoueurDAO {

	public static String POINTS = "points";
	public static String ASC = "", DESC = "desc";
	
	public static List<Joueur> getListe(String tri,String ordre) {
		// getting data
				List<Joueur> joueurs = new ArrayList<Joueur>();
				try {
					Statement stat = DB.getConnection().createStatement();
					String orderBy = "nom";
					if (tri != null) {
						orderBy = tri;
					}
					ResultSet res = stat.executeQuery("select * from joueurs ORDER BY "+orderBy+ " "+ordre);
					while (res.next()) {
						joueurs.add(creerJoueur(res));
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				return joueurs;
	}
	
	public static void ajouterPoints(int idJoueur,int points,String raison) {
		try {
			PreparedStatement statement = DB.getConnection().prepareStatement("UPDATE joueurs SET points = points + ? WHERE id = ?");
			statement.setInt(1,points);
			statement.setInt(2, idJoueur);
			statement.executeUpdate();
			
			TransactionDAO.ajouterTransaction(idJoueur, points, raison);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static List<Joueur> getListe() {
		return getListe(null,ASC);
	}
	
	private static Joueur creerJoueur(ResultSet res) throws SQLException {
		return new Joueur(res.getInt("id"),res.getString("nom"),res.getString("pin"),res.getInt("points"));
	}
	
	public static void createNewJoueur() {
		try {
			PreparedStatement prep = DB.getConnection()
					.prepareStatement("insert into joueurs(nom,pin,points) values(?,?,?);");
			prep.setString(1, "Nouveau joueur");
			prep.setInt(2, (int) (Math.random()*8999) + 1000);
			prep.setInt(3,Prefs.prefs.getInt(Prefs.ETINCELLESDEPART, 1000));
			prep.execute();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void delete(int id) {
		try {
			PreparedStatement prep = DB.getConnection()
					.prepareStatement("DELETE FROM joueurs WHERE id = ?");
			prep.setInt(1,id);
			prep.execute();
			PreparedStatement prep2 = DB.getConnection()
					.prepareStatement("DELETE FROM reponses WHERE idJoueur = ?");
			prep2.setInt(1,id);
			prep2.execute();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void insertOrUpdate(Joueur j) {
		try {
			PreparedStatement prep = DB.getConnection()
					.prepareStatement("replace into joueurs(nom,pin,points,id) values(?,?,?,?);");
			prep.setString(1, j.getNom());
			prep.setString(2, j.getPin());
			prep.setInt(3,j.getPoints());
			prep.setInt(4, j.getId());
			prep.execute();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
			
	}
	
	public static Joueur getById(int id) {
		try {
			Statement stat = DB.getConnection().createStatement();
			ResultSet res = stat.executeQuery("select * from joueurs WHERE id = "+id);
			while (res.next()) {
				return creerJoueur(res);
			}
			return null;
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Joueur[] getTableau() {
		List<Joueur> joueurs = getListe();
		Joueur[] tableau = new Joueur[joueurs.size()];
		for (int i = 0; i < joueurs.size(); i++) {
			tableau[i] = joueurs.get(i);
		}
		return tableau;
	}
	
	public static boolean connexion(String nom,String pin) {
		
		return false;
	}

}
