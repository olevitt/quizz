package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import javax.swing.JOptionPane;

import core.Quizz;
import csv.CSV;

public class DB {


	private static Connection con;
	public static final int VERSION = 1;
	
	public static synchronized Connection getConnection() {
		try {
			if (con == null) init();
			return con;
		}
		catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Erreur de base de donnée, l'application va fermer");
			System.exit(0);
			return null;
		}
	}
	
	private static void init() throws Exception {

		// sqlite driver
		Class.forName("org.sqlite.JDBC");
		// database path, if it's new database,
		// it will be created in the project folder
		con = DriverManager.getConnection("jdbc:sqlite:mydb.db");
		Statement stat = con.createStatement();

		//PreparedStatement statement = con.prepareStatement("SELECT version FROM settings");
		//statement.executeQuery();
		
		//creating table
		stat.executeUpdate("create table if not exists joueurs(id INTEGER PRIMARY KEY,"
				+ "nom varchar(50),"
				+ "pin varchar(5),"
				+ "points INT);");
		
		stat.executeUpdate("create table if not exists quizz(id INTEGER PRIMARY KEY,"
				+ "jour DATE,"
				+ "question text(255),"
				+ "reponsea text(255),"
				+ "reponseb text(255),"
				+ "reponsec text(255),"
				+ "reponsed text(255),"
				+ "bonneReponse INT)");
		
		stat.executeUpdate("create table if not exists reponses(idQuestion INTEGER NOT NULL,"
				+ "idJoueur INTEGER NOT NULL,"
				+ "jetonsA INTEGER,"
				+ "jetonsB INTEGER,"
				+ "jetonsC INTEGER,"
				+ "jetonsD INTEGER," +
				"PRIMARY KEY (idQuestion, idJoueur))");
		
		stat.executeUpdate("create table if not exists traitement(idQuestion INTEGER PRIMARY KEY,"
				+ "jourTraitement DATE)");
		
		stat.executeUpdate("create table if not exists transactions(idTransaction INTEGER PRIMARY KEY,"
				+ "jourTransaction DATE," 
				+ "idJoueur INTEGER," 
				+ "montant INTEGER," +
				"raison text(255))");
		
		TraitementDAO.traiterQuizzEnAttente();
	}
	
	
}
