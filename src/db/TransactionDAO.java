package db;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import preferences.Prefs;

import core.Transaction;

public class TransactionDAO {

	public static void ajouterTransaction(int idJoueur, int montant, String raison) {
		try {
			PreparedStatement stat = DB.getConnection().prepareStatement("INSERT INTO transactions(idJoueur,montant,raison,jourTransaction) VALUES(?,?,?,?)");
			stat.setInt(1, idJoueur);
			stat.setInt(2, montant);
			stat.setString(3, raison);
			stat.setDate(4, new Date(new java.util.Date().getTime()));
			stat.execute();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public static List<Transaction> getAll(int idJoueur) {
		List<Transaction> transactions = new ArrayList<Transaction>();
		try {
			Statement stat = DB.getConnection().createStatement();
			String filtre = "";
			if (idJoueur != 0) {
				filtre = "AND idJoueur = "+idJoueur;
			}
			ResultSet res = stat.executeQuery("select transactions.raison, transactions.montant, transactions.jourTransaction, joueurs.nom from transactions, joueurs WHERE joueurs.id = transactions.idJoueur "+filtre+" ORDER BY jourTransaction DESC LIMIT "+Prefs.prefs.getInt(Prefs.LIGNESHISTO, 100));
			while (res.next()) {
				transactions.add(creerTransaction(res));
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return transactions;
	}
	
	private static Transaction creerTransaction(ResultSet res) {
		try {
			return new Transaction(res.getString("raison"),res.getString("nom"),res.getDate("jourTransaction"),res.getInt("montant"));
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
