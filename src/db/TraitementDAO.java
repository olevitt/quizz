package db;

import java.io.ObjectInputStream.GetField;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;

public class TraitementDAO {

	public static boolean isTraite(int idQuizz) {
		try {
			ResultSet res = DB.getConnection().createStatement().executeQuery("select * from traitement WHERE idQuestion = "+idQuizz);
			while (res.next()) {
				return true;
			}
			return false;
		}
		catch (Exception e) {
			return false;
		}
		
	}
	
	public static void setTraite(int idQuizz) {
		try {
			PreparedStatement prep = DB.getConnection()
					.prepareStatement("insert into traitement(idQuestion,jourTraitement) values(?,?);");
			prep.setInt(1, idQuizz);
			prep.setDate(2, new java.sql.Date(new Date().getTime()));
			prep.execute();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void traiterQuizzEnAttente() {
		try {
			PreparedStatement statement = DB.getConnection().prepareStatement("SELECT quizz.id,quizz.jour FROM quizz WHERE quizz.id NOT IN (SELECT idQuestion FROM traitement)");
			ResultSet res = statement.executeQuery();
			while (res.next()) {
				Date date = res.getDate(2);
				if (date.compareTo(QuizzDao.getDebut().getTime()) < 0) {
					System.out.println("Traitement de "+res.getInt(1));
					QuizzDao.traitementQuizz(res.getInt(1));
				}
			}
		}
		catch (Exception e) {
			System.out.println("Impossible de traiter les quizz en attente");
			e.printStackTrace();
		}
	}
}
