package preferences;

import java.util.prefs.Preferences;

public class Prefs {

	public static String MAUVAISEREPONSE = "mauvaiseReponse",
			BONNEREPONSE = "bonneReponse",
			ETINCELLESDEPART = "etincellesDepart",
			AVISDUPUBLIC = "avisDuPublic",
			CINQUANTECINQUANTE = "cinquanteCinquante",
			CHECKBOX5050 = "checkBox5050",
			CHECKBOXPUBLIC = "checkBoxPublic",
			MINPUBLIC = "miniPublic",
			LIGNESHISTO = "lignesHisto";
			
	public static Preferences prefs = Preferences.userNodeForPackage(Prefs.class);

}
