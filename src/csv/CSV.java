package csv;

import java.io.FileWriter;
import java.util.List;

import au.com.bytecode.opencsv.CSVWriter;
import core.Joueur;
import core.Transaction;
import db.JoueurDAO;
import db.TransactionDAO;

public class CSV {

	public static void exportClassement() {
		try {
			CSVWriter writer = new CSVWriter(new FileWriter("classement.csv"), ';');
			String[] entries = "Place#Nom#Etincelles".split("#");
			writer.writeNext(entries);
			List<Joueur> joueurs = JoueurDAO.getListe(JoueurDAO.POINTS,JoueurDAO.DESC);
			int i = 1;
			for (Joueur j : joueurs) {
				writer.writeNext(new String[] {i+"",j.getNom(),""+j.getPoints()});
				i++;
			}
			
			writer.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void exportHistorique(int idJoueur) {
		try {
			CSVWriter writer = new CSVWriter(new FileWriter("historique.csv"), ';');
			String[] entries = "Joueur#Date#Montant#Raison".split("#");
			writer.writeNext(entries);
			List<Transaction> transactions = TransactionDAO.getAll(idJoueur);
			int i = 1;
			for (Transaction t : transactions) {
				writer.writeNext(new String[] {t.getNomJoueur(),t.getDate().toLocaleString(),""+t.getMontant(),t.getRaison()});
				i++;
			}
			
			writer.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
